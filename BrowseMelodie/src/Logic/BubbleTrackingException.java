package Logic;

public class BubbleTrackingException extends Exception {

	private static final long serialVersionUID = 1L;

	public BubbleTrackingException(String message, Throwable cause) {
		super(message, cause);
	}

	public BubbleTrackingException(String message) {
		super(message);
	}

	public BubbleTrackingException() {
		super();
	}

	public BubbleTrackingException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public BubbleTrackingException(Throwable cause) {
		super(cause);
	}
}