package Logic;

import java.util.LinkedList;
import java.util.List;

public class Database {
	
	private String projectDirectory = System.getProperty("user.dir");;
	private List<Bubble> bubbles = new LinkedList<>();
	private List<String> tags = new LinkedList<>();
	
	public List<Bubble> getBubbles() {
		return bubbles;
	}

	public void setBubbles(List<Bubble> bubbles) {
		this.bubbles = bubbles;
	}
	
	public void removeBubble(Bubble bubble) {
		bubbles.remove(bubble);
	}
	
	public List<String> getTags(){
		return tags;
	}
	
	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public void removeTag(String tag){
			tags.remove(tag);
	}

	public String getProjectDirectory() {
		return projectDirectory;
	}
	
}
