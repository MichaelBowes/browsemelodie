package Logic;

import java.io.IOException;
import java.util.List;
import java.util.Set;

public interface DBService {

	/**
	 * Loads all saved tags and tracks on startup.
	 */
	public void initializeBrowseMelodie(String loadPath);
	
	/**
	 * Saves all tags and tracks persistently in 2 separate CSV files.
	 * Tags are saved as String in a CSV file.
	 * Tracks are saved as reference path in a CSV file.
	 */
	public void savePersistently(String savePath) throws IOException;
	
	/**
	 * Creates a new {@link Bubble}.
	 * 
	 */
	public Bubble createBubble(String path, Set<String> tags)
			throws BubbleTrackingException;
	/**
	 * Fetches an array of the {@link Issue}s attached to the Project.
	 * 
	 * @return the {@link Issue}s of the current Project.
	 */
	public List<Bubble> getBubbles();

	/**
	 * Deletes an {@link Issue} by ID. Also deletes all references to this
	 * {@link Issue} in other {@link Issue}s.
	 * 
	 * @param id
	 *            the ID of the {@link Issue} to be deleted. Must not be null or
	 *            empty. Must be a valid ID.
	 * @throws IssueTrackingException
	 *             {@link Issue} with given ID does not exist in project or one
	 *             of the dependent {@link Issue}s caused a problem during the
	 *             deletion of the references.
	 */
	public void removeBubble(String id) throws BubbleTrackingException, IOException;
	
	public void removeTag(String string) throws BubbleTrackingException, IOException;
	
	public List<String> getTags();
	
	public void addTag(String tag) throws BubbleTrackingException;
	
	/**
	 * generates an integer value between 0 and i
	 * */
	public int getRandomInt(int i);
	
}

