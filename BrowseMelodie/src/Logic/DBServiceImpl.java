package Logic;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.media.Media;
import javafx.util.Duration;
import Logic.BubbleVisual;

public class DBServiceImpl implements DBService {

	private Database db; // database with all tracks
	private Duration duration; // track duration

	public DBServiceImpl() {
		db = new Database();
	}

	@Override
	public Bubble createBubble(String path, Set<String> tags) throws BubbleTrackingException {

		String id = Integer.toString(getRandomInt(2000));
		String title = "default";
		String author = "default";

		File file = new File(path);
		String uri = file.toURI().toString();
		Media music = new Media(uri);

		BubbleVisual visual = new BubbleVisual();
		duration = music.getDuration();

		String exp = ".*\\\\(.*?)\\s-" + "\\s([^-]+)\\."; // Author + Title
		Pattern pattern = Pattern.compile(exp);

		Matcher matcher = pattern.matcher(path);
		while (matcher.find()) {
			author = matcher.group(1);
			title = matcher.group(2);
		}
		
		if(tags.isEmpty()) {
			tags.add(db.getTags().get(0));
		}
		
		Bubble bubble = new Bubble(id, title, author, music, visual, path, tags, duration);
		List<Bubble> b = db.getBubbles();
		b.add(bubble);
		db.setBubbles(b);

		return bubble;
	}

	@Override
	public List<Bubble> getBubbles() {
		return db.getBubbles();
	}

	@Override
	public List<String> getTags() {
		return db.getTags();
	}

	@Override
	public void removeBubble(String id) throws BubbleTrackingException, IOException {
		List<Bubble> b = db.getBubbles();
		for (Bubble bub : b) {
			if (bub.getId() == id) {
				db.removeBubble(bub); // temporary removal for GUI

				List<String> bubbleList = new LinkedList<>();
				// persistant removal from save data
				Path folder = Paths.get(db.getProjectDirectory().replace("\\BrowseMelodie", ""));
				try (DirectoryStream<Path> stream = Files.newDirectoryStream(folder)) {

					for (Path path : stream) {
						if (path.getFileName().toString().endsWith("_tracks.csv")) {
							List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);
							for (String line : lines) {
								if (!line.contains(bub.getPath())) {
									bubbleList.add(line);
								}
							}
						}

					}
					System.out.println("BubbleList: " + bubbleList);

					BufferedWriter trackWriter = new BufferedWriter(
							new FileWriter(db.getProjectDirectory() + "_tracks.csv"));
					for (String s : bubbleList) {
						trackWriter.write(s + System.getProperty("line.separator"));
					}
					trackWriter.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

			}

		}
	}

	@Override
	public void removeTag(String name) throws BubbleTrackingException, IOException {
		List<String> list = db.getTags();

		if(!name.equals("none")) { //The default none-Tag musn't be deleted.
			
			//remove tag from tags.csv
			for (String s : list) {
				if (s == name) {
					//runLater to prevent concurrency clash with updateTagTab in DeleteTagViewController
					Platform.runLater(new Runnable() {
	                    @Override
	                    public void run() {
						db.removeTag(name);

						List<String> tagList = new LinkedList<>();
						// persistant removal from save data
						Path folder = Paths.get(db.getProjectDirectory().replace("\\BrowseMelodie", ""));
						try (DirectoryStream<Path> stream = Files.newDirectoryStream(folder)) {

							for (Path path : stream) {
								if (path.getFileName().toString().endsWith("_tags.csv")) {
									List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);
									for (String line : lines) {
										if (!line.contains(name)) {
											tagList.add(line);
										}
									}
								}

							}

							BufferedWriter trackWriter = new BufferedWriter(
									new FileWriter(db.getProjectDirectory() + "_tags.csv"));
							for (String str : tagList) {
								trackWriter.write(str + System.getProperty("line.separator"));
							}
							trackWriter.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
	                    }
	                });
					}

				}
			
		}
		
	}

	@Override
	public void addTag(String tag) {
		if(!tag.equals("")&&!tag.equals("none")) {
			List<String> tags = db.getTags();
			tags.add(tag);
		} else {
			Alert alert = new Alert(AlertType.ERROR, "Tagname is empty");
			alert.showAndWait();
		}
	}

	@Override
	public int getRandomInt(int i) {
		double a = Math.floor(Math.random() * Math.floor(i));

		return (int) a;
	}

	@Override
	public void initializeBrowseMelodie(String loadPath) {
		Path folder = Paths.get(loadPath.replace("\\BrowseMelodie", ""));
		try (DirectoryStream<Path> stream = Files.newDirectoryStream(folder)) {
			for (Path path : stream) {
				if (path.getFileName().toString().endsWith("_tags.csv")) {
					List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);
					db.setTags(lines);
				}

				if (path.getFileName().toString().endsWith("_tracks.csv")) {
					List<Bubble> bubbles = new LinkedList<Bubble>();
					List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);
					
					String tempBubbs = "";
					
					for (String line : lines) {
						Set<String> tempTags = new HashSet<>();
						String exp = "^(.*.mp3)\\s,\\s(.*)";
						Pattern pattern = Pattern.compile(exp);
						Matcher matcher = pattern.matcher(line);
						
						while (matcher.find()) {
							tempBubbs = matcher.group(1);
							String[] parts = matcher.group(2).split(",");
							for(int i=0;i<parts.length;i++) {
									tempTags.add(parts[i].trim());
							}
						}

						if(tempTags.isEmpty()) {
							System.out.println("tagsEmpty!");
							tempTags.add(db.getTags().get(0));
							bubbles.add(createBubble(tempBubbs, tempTags));
						} else {
							bubbles.add(createBubble(tempBubbs, tempTags));
						}

					}
					
					db.setBubbles(bubbles);
				}

			}
		} catch (IOException | BubbleTrackingException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void savePersistently(String savePath) throws IOException {
		BufferedWriter tagWriter = new BufferedWriter(new FileWriter(savePath + "_tags.csv"));
		BufferedWriter trackWriter = new BufferedWriter(new FileWriter(savePath + "_tracks.csv"));

		try {
			File tagfile = new File(savePath + "_tags.csv");
			tagfile.getParentFile().mkdirs();
			tagfile.createNewFile(); // creates file iff it does not already exist.
			File trackfile = new File(savePath + "_tracks.csv");
			trackfile.getParentFile().mkdirs();
			trackfile.createNewFile(); // creates file iff it does not already exist.

			List<Bubble> bubbles = db.getBubbles();
			List<String> tags = db.getTags();

			for (String t : tags) {
				tagWriter.write(t + System.getProperty("line.separator"));
			}

			for (Bubble b : bubbles) {
				Set<String> bubbleTags = b.getTags();
				trackWriter.write(b.getPath());
				for(String tag : bubbleTags) {
					trackWriter.write(" , " + tag);
				}
				trackWriter.write(System.getProperty("line.separator"));
			}

		} catch (IOException e) {
			System.out.println("Could not create file");
		} finally {
			tagWriter.close();
			trackWriter.close();
		}
	}

	public Database getDatabase() {
		return db;
	}

}
