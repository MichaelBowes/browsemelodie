package Logic;

import java.util.Set;

import javafx.scene.media.Media;
import javafx.util.Duration;

/**
 * An object that contains a music track
 * and its associated tracks.
 * 
 * */

public class Bubble {

	public Bubble(String id, String title, String author, Media audio, BubbleVisual visual,
			String path, Set<String> tags, Duration duration) {
		super();
		//tangible attributes, visible in the view
		this.id = id;
		this.title = title;
		this.author = author;
		this.tags = tags;
		this.visual = visual;
		//intangible attributes, not visible in the view
		this.audio = audio;
		this.path = path;
	}
	private String id;
	private String title;
	private String author;
	private Media audio;
	private Set<String> tags;
	private BubbleVisual visual;
	private String path;
	private String duration;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public Media getAudio() {
		return audio;
	}
	public void setAudio(Media audio) {
		this.audio = audio;
	}
	public Set<String> getTags() {
		return tags;
	}
	public void setTags(Set<String> tags) {
		this.tags = tags;
	}
	public BubbleVisual getVisual() {
		return visual;
	}
	public void setVisual(BubbleVisual visual) {
		this.visual = visual;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	
}
