package Gui;

import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.event.EventHandler;

public class AddTagView extends Stage {

	private final TagViewController controller;

	/**
	 * A {@link TextField} that expects the user to input a name for the tag to be
	 * added.
	 */
	private TextField tagField;

	public AddTagView(TagViewController controller) {
		this.controller = controller;
		Pane rootPane = createRootPane();
		// Adding the window title
		this.setTitle("Add new Tag");
		// Setting the scene
		this.setScene(new Scene(rootPane, 500, 100));
		this.setMinWidth(500);
		this.setMinHeight(100);
	}

	/**
	 * Creates the root pane with all content to be displayed.
	 * 
	 * @return the {@link BorderPane} that serves as root for all other panes.
	 */
	private Pane createRootPane() {
		BorderPane root = new BorderPane();
		root.setPadding(new Insets(10, 0, 0, 10));

		// Setting elements of the root pane
		root.setCenter(createInputPane());
		root.setBottom(createButtonPane());

		root.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

		return root;
	}

	/**
	 * Creates the {@link AnchorPane} that displays the input field.
	 */
	private Node createInputPane() {
		AnchorPane anchorPane = new AnchorPane();
		Label nameLabel = new Label("Name:");
		tagField = new TextField();
		tagField.addEventHandler(KeyEvent.KEY_PRESSED, (key) -> {
			if (key.getCode() == KeyCode.ENTER) {
				controller.addTag(tagField.getText());
				controller.closeView();
			}
		});

		anchorPane.getChildren().addAll(nameLabel, tagField);

		return anchorPane;
	}

	/**
	 * Creates the {@link AnchorPane} with the buttons.
	 */
	private Node createButtonPane() {

		Button cancelButton = new Button("Back");
		cancelButton.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				controller.closeView();
			}
		});

		Button addTagButton = new Button("Confirm");
		addTagButton.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				controller.addTag(tagField.getText());
				controller.closeView();
			}
		});

		AnchorPane anchorPane = new AnchorPane();
		HBox box = new HBox();
		box.getChildren().addAll(cancelButton, addTagButton);
		anchorPane.getChildren().addAll(box);
		box.setPadding(new Insets(0, 10, 10, 0));
		box.setSpacing(10);
		AnchorPane.setRightAnchor(box, 10.0);

		return anchorPane;
	}

}
