package Gui;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import Logic.Bubble;
import Logic.BubbleTrackingException;
import Logic.DBServiceImpl;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.scene.media.MediaPlayer;

public class MainModelImpl extends MainModel {

	/**
	 * Currently loaded Project.
	 */
	private DBServiceImpl dbService;

	/**
	 * List containing all bubbles from the currently loaded project.
	 */
	private SimpleListProperty<Bubble> bubbles;
	/**
	 * List containing all tags from the currently loaded project.
	 */
	private SimpleListProperty<String> tags;

	private Bubble selectedBubble;
	
	private MediaPlayer currentMediaPlayer;

	public MainModelImpl() {
		dbService = new DBServiceImpl();
		bubbles = new SimpleListProperty<Bubble>(FXCollections.observableArrayList());
		tags = new SimpleListProperty<String>(FXCollections.observableArrayList());
	}

	@Override
	public void initialize() throws BubbleTrackingException {
		dbService.initializeBrowseMelodie(dbService.getDatabase().getProjectDirectory());
		updateBubbles();
	}

	@Override
	public SimpleListProperty<Bubble> getBubbleListProperty() {
		return bubbles;
	}
	
	@Override
	public SimpleListProperty<String> getTagListProperty(){
		return tags;
	}

	@Override
	public void updateBubbles() {
		if (dbService != null) {
			this.bubbles.clear();
			this.bubbles.set(FXCollections.observableArrayList(dbService.getBubbles()));
		}
	}
	
	@Override
	public void updateTags() {
		if (dbService != null) {
			this.tags.clear();
			this.tags.set(FXCollections.observableArrayList(dbService.getTags()));
		}
	}

	@Override
	public void setSelectedBubble(Bubble bubble) {
		selectedBubble = bubble;
	}

	@Override
	public Bubble getSelectedBubble() {
		return selectedBubble;
	}

	@Override
	public void addBubble(String path) throws BubbleTrackingException {
		Set<String> tags = new HashSet<>();
		dbService.createBubble(path, tags);
		updateBubbles();
		try {
			dbService.savePersistently(dbService.getDatabase().getProjectDirectory());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void addTag(String tag) throws BubbleTrackingException {
		dbService.addTag(tag);
		updateTags();
		try {
			dbService.savePersistently(dbService.getDatabase().getProjectDirectory());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void deleteSelectedBubble() throws BubbleTrackingException {
		if (selectedBubble != null) {
			try {
				dbService.removeBubble(selectedBubble.getId());
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (bubbles.remove(selectedBubble)) {
				selectedBubble = null;
			}
		}
	}
	
	@Override
	public void deleteSelectedTag(String tag) throws BubbleTrackingException {
		if(tag != null) {
			try {
				dbService.removeTag(tag);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void savePersistently() throws IOException {
		dbService.savePersistently(dbService.getDatabase().getProjectDirectory());
	}
	
	@Override
	public List<String> getTags() {
		return dbService.getTags();
	}
	
	@Override
	public void setMediaPlayer(MediaPlayer mp) {
		currentMediaPlayer = mp;
	}
	
	@Override
	public MediaPlayer getMediaPlayer() {
		return currentMediaPlayer;
	}

}