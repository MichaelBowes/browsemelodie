package Gui;

import Logic.Bubble;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class AssociateTagsView extends Stage {

	private AssociateController controller;

	private TextField tagField;
	private Bubble bubble;
	private ComboBox associateBox;

	public AssociateTagsView(AssociateController controller, Bubble bubble) {
		this.bubble = bubble;
		this.controller = controller;
		Pane rootPane = createRootPane();
		// Adding the window title
		this.setTitle("Add Tag to Bubble");
		// Setting the scene
		this.setScene(new Scene(rootPane, 500, 100));
		this.setMinWidth(500);
		this.setMinHeight(100);
	}

	/**
	 * Creates the root pane with all content to be displayed.
	 * 
	 * @return the {@link BorderPane} that serves as root for all other panes.
	 */
	private Pane createRootPane() {
		BorderPane root = new BorderPane();
		root.setPadding(new Insets(10, 0, 0, 10));

		// Setting elements of the root pane
		root.setCenter(createInputPane());
		root.setBottom(createButtonPane());

		root.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

		return root;
	}

	/**
	 * Creates the {@link AnchorPane} that displays the input field.
	 */
	private Node createInputPane() {
		AnchorPane anchorPane = new AnchorPane();

		associateBox = new ComboBox();
		associateBox.getItems().addAll(controller.getTags());

		Label nameLabel = new Label("Tags:");

		anchorPane.getChildren().addAll(nameLabel, associateBox);

		return anchorPane;
	}

	/**
	 * Creates the {@link AnchorPane} with the buttons.
	 */
	private Node createButtonPane() {

		Button cancelButton = new Button("Back");
		cancelButton.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				controller.closeView();
			}
		});

		Button tagButton = new Button("tag");
		tagButton.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				if(!associateBox.getSelectionModel().isEmpty()) {
					controller.associateTag(bubble, associateBox.getValue().toString());
					controller.closeView();
				}
			}
		});

		Button untagButton = new Button("untag");
		untagButton.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				if(!associateBox.getSelectionModel().isEmpty()) {
				controller.deassociateTag(bubble, associateBox.getValue().toString());
				controller.closeView();
				}
			}
		});

		AnchorPane anchorPane = new AnchorPane();
		HBox box = new HBox();
		box.getChildren().addAll(cancelButton, untagButton, tagButton);
		anchorPane.getChildren().addAll(box);
		box.setPadding(new Insets(0, 10, 10, 0));
		box.setSpacing(10);
		AnchorPane.setRightAnchor(box, 10.0);

		return anchorPane;
	}

}
