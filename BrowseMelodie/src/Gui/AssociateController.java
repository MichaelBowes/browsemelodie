package Gui;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import Logic.Bubble;
import javafx.stage.Modality;

public class AssociateController {

	MainModelImpl model;
	MainView mainView;
	private AssociateTagsView view;
	
	public AssociateController(MainModelImpl model, MainView view) {
		this.model = model;
		this.mainView = mainView;
	};
	
	public void createView(Bubble bubble) {
			view = new AssociateTagsView(this, bubble);
			view.initModality(Modality.APPLICATION_MODAL);
			view.showAndWait();
	}
	
	public void closeView() {
		view.close();
	}
	
	public List<String> getTags() {
		return model.getTags();
	}
	
	public void associateTag(Bubble bubble, String tag) {
		Set<String> tags;
		tags = bubble.getTags();
		
		tags.add(tag);
		bubble.setTags(tags);
		if(tags.contains("none")) {
			tags.remove("none");
		}
		model.updateBubbles();
		try {
			model.savePersistently();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void deassociateTag(Bubble bubble, String tag) {
		Set<String> tags;
		tags = bubble.getTags();
		
		tags.remove(tag);
		bubble.setTags(tags);
		if(tags.isEmpty()) {
			tags.add("none");
		}
		model.updateBubbles();
		try {
			model.savePersistently();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
