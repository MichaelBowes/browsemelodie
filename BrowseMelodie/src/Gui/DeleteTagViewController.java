package Gui;

import java.io.IOException;

import Logic.BubbleTrackingException;
import javafx.application.Platform;
import javafx.stage.Modality;

public class DeleteTagViewController {

	private final MainModelImpl model;
	private DeleteTagView view;
	private MainView mainView;

	public DeleteTagViewController(MainModelImpl model, MainView mainView) {
		this.model = model;
		this.mainView = mainView;
	}

	public void createView(String tag) {
		view = new DeleteTagView(this, tag);
		view.initModality(Modality.APPLICATION_MODAL);
		view.showAndWait();
	};

	public void closeView() {
		view.close();
	};

	public void removeTag(String tag) {
		try {
			model.deleteSelectedTag(tag);

			// runLater to prevent concurrency clash with removeTag in DBServiceImpl
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					mainView.updateTagTab();
					mainView.updateBubbleTags(tag);
					model.updateBubbles();
					try {
						model.savePersistently();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			});

		} catch (BubbleTrackingException e) {
			e.printStackTrace();
		}
	};

}
