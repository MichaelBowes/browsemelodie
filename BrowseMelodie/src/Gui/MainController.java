package Gui;

import java.util.List;

import Logic.BubbleTrackingException;


/**
 * 
 * Holds the model and the view.<br>
 * Receives updates from the view and tells the model to change.<br>
 * Receives updates from the model and tells the view to change.
 * 
 * @author Flevalt
 *
 */
public class MainController {

	private final MainModelImpl model;
	private MainView view;
	
	public MainController(MainModelImpl model) {
		this.model = model;
	}
	
	/**
	 * Tells the model to initialize the {@link Database} and creates the {@link MainView}.
	 */
	public void startUp(){
		try {
			model.initialize();
		} catch (BubbleTrackingException e) {
			view.showErrorMessage(e.getMessage());
			view.close();
		}
		view = new MainView(model, this);
		view.showAndWait();
	}	
	
	/**
	 * Tells the view to update the Found Bubble Tab.
	 * @param tags The tag(s) used for the search.
	 */
	public void searchBubbleByTag(List<String> tags) {
		view.updateResultTable(tags);
	}
	
	public boolean endApplicationDialog() {
		boolean result = view.showConfirmationAlert("Close the application?");
		if(result) {
			view.close();
		}
		return result;
	}
	
	/**
	 * Tells the main view to open a file chooser.
	 * The chosen file then gets added as Bubble to the model.
	 */
	public void importBubbleDialog() {
		String path = view.showFileDialog();
		if(path != null) {
			try {
				model.addBubble(path);
			} catch (BubbleTrackingException e) {
				view.showErrorMessage(e.getMessage());
			}
		}else {}			
	}
	
	/**
	 * Tells the model to delete the selected Bubble.
	 */
	public void deleteSelectedBubble() {
		try {
			model.deleteSelectedBubble();
		} catch (BubbleTrackingException e) {
			view.showErrorMessage(e.getMessage());
		}
	}

}