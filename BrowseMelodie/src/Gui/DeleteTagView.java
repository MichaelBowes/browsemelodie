package Gui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class DeleteTagView extends Stage {

	private final DeleteTagViewController controller;
	private String tag;

	public DeleteTagView(DeleteTagViewController controller, String tag) {
		this.tag = tag;
		this.controller = controller;
		Pane rootPane = createRootPane();
		// Adding the window title
		this.setTitle("Delete the tag?");
		// Setting the scene
		this.setScene(new Scene(rootPane, 500, 100));
		this.setMinWidth(500);
		this.setMinHeight(100);
	}

	/**
	 * Creates the root pane with all content to be displayed.
	 * 
	 * @return the {@link BorderPane} that serves as root for all other panes.
	 */
	private Pane createRootPane() {
		BorderPane root = new BorderPane();
		root.setPadding(new Insets(10, 0, 0, 10));

		// Setting elements of the root pane
		root.setCenter(createButtonPane());

		root.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

		return root;
	}

	/**
	 * Creates the {@link AnchorPane} with the buttons.
	 */
	private Node createButtonPane() {

		Button cancelButton = new Button("Back");
		cancelButton.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				controller.closeView();
			}
		});

		Button deleteTagButton = new Button("Confirm");
		deleteTagButton.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				controller.removeTag(tag);
				controller.closeView();
			}
		});

		AnchorPane anchorPane = new AnchorPane();
		HBox box = new HBox();
		box.getChildren().addAll(cancelButton, deleteTagButton);
		anchorPane.getChildren().addAll(box);
		box.setPadding(new Insets(0, 10, 10, 0));
		box.setSpacing(10);
		AnchorPane.setRightAnchor(box, 10.0);

		return anchorPane;
	}
}
