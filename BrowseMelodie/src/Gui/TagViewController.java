package Gui;

import Logic.BubbleTrackingException;
import javafx.stage.Modality;

public class TagViewController {

	private final MainModelImpl model;
	private AddTagView view;
	private MainView mainView;

	public TagViewController(MainModelImpl model, MainView mainView) {
		this.model = model;
		this.mainView = mainView;
	}

	public void createView() {

		view = new AddTagView(this);
		view.initModality(Modality.APPLICATION_MODAL);
		view.showAndWait();

	};

	public void closeView() {
		view.close();
	};

	public void addTag(String tag) {

		try {
			model.addTag(tag);
			mainView.updateTagTab();
		} catch (BubbleTrackingException e) {
			e.printStackTrace();
		}
	};

}
