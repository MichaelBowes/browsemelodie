package Gui;

import java.io.IOException;
import java.util.List;
import java.util.Observable;

import javafx.scene.media.MediaPlayer;
import Logic.Bubble;
import Logic.BubbleTrackingException;
import Logic.DBServiceImpl;
import javafx.beans.property.SimpleListProperty;

/**
 * 
 * Serves as Memory for the BrowseMelodie Software.<br>
 * Stores the {@link DBServiceImpl} which contains the database,<br>
 * all {@link Bubble}s, {@link Tag}s, the currently selected Bubble,<br>
 * the current {@link MediaPlayer} and the project's path.
 * 
 * @author Flevalt
 *
 */

public abstract class MainModel extends Observable {

	/**
	 * Initializes the model by loading all save files<br>
	 * and updating the bubble view.
	 * 
	 * @throws BubbleTrackingException
	 *             If the IssueTracker could not be initialized.
	 */
	public abstract void initialize() throws BubbleTrackingException;

	/**
	 * Returns the {@link Tag}s.<br>
	 * 
	 * @return All Tags.
	 */
	public abstract SimpleListProperty<String> getTagListProperty();

	/**
	 * Returns the {@link Bubble}s.<br>
	 * 
	 * @return All Bubbles.
	 */
	public abstract SimpleListProperty<Bubble> getBubbleListProperty();

	/**
	 * Clears the currently set Bubbles in the model<br>
	 * and reloads them from the {@link DBServiceImpl}.
	 */
	public abstract void updateBubbles();

	/**
	 * Clears the currently set Tags in the model<br>
	 * and reloads them from the {@link DBServiceImpl}.
	 */
	public abstract void updateTags();

	/**
	 * Sets the currently selected Bubble in the model.
	 * 
	 * @param bubble
	 *            that was last clicked on in the Bubble Overview Tab.
	 */
	public abstract void setSelectedBubble(Bubble bubble);

	/**
	 * Gets the currently selected Bubble.
	 * 
	 * @return Bubble that was last clicked on in the Bubble Overview Tab
	 */
	public abstract Bubble getSelectedBubble();

	/**
	 * Creates a new {@link Bubble} in the {@link DBServiceImpl}.<br>
	 * Bubbles start with a default none-tag associated to them.<br>
	 * Also updates the Bubble Overview Tab and saves the Bubbles persistently.
	 * 
	 * @param path
	 *            the directory the track needed to create the Bubble is located in.
	 * @throws BubbleTrackingException
	 *             if the parameters are invalid.
	 */
	public abstract void addBubble(String path) throws BubbleTrackingException;

	/**
	 * 
	 * Creates a new {@link Tag} in the {@link DBServiceImpl}.<br>
	 * Also updates the Tags Tab and saves the Tags persistently.
	 * 
	 * @param tag
	 *            the name of the Tag to be added.
	 * @throws BubbleTrackingException
	 *             if the parameters are invalid.
	 */
	public abstract void addTag(String tag) throws BubbleTrackingException;

	/**
	 * Deletes a {@link Bubble} from the {@link DBServiceImpl}.
	 * 
	 */
	public abstract void deleteSelectedBubble() throws BubbleTrackingException;

	/**
	 * Deletes a {@link Tag} from the {@link DBServiceImpl}.
	 * 
	 */
	public abstract void deleteSelectedTag(String tag) throws BubbleTrackingException;

	/**
	 * Calls the {@link DBServiceImpl} to save Tags and Bubbles persistently.
	 * 
	 */
	public abstract void savePersistently() throws IOException;

	public abstract List<String> getTags();

	public abstract void setMediaPlayer(MediaPlayer mp);

	public abstract MediaPlayer getMediaPlayer();
}