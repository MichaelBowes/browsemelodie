package Gui;

import java.io.File;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import Gui.MainModelImpl;
import Gui.MainController;
import Logic.Bubble;
import Utility.ToolTipDefaultsFixer;

import javafx.application.Platform;
import javafx.beans.binding.StringBinding;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.media.MediaPlayer;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Callback;
import javafx.util.Duration;

public class MainView extends Stage {

	private final MainModelImpl model;
	private final MainController controller;
	private final Scene scene;
	GridPane tagGrid; 
	GridPane resultGrid;
	TabPane tabs;
	Tab searchResultTab;
	TextArea duration;
	boolean currentlyPlaying = false;

	public MainView(MainModelImpl model, MainController controller) {
		this.model = model;
		this.controller = controller;
		Pane rootPane = createRootPane();
		// Binding the title property
		setTitleBinding();
		// setting the scene
		scene = new Scene(rootPane, 550, 300);
		this.setScene(scene);
		this.setOnCloseRequest(new EventHandler<WindowEvent>() {
			public void handle(WindowEvent event) {
				Platform.exit();
				if (!controller.endApplicationDialog()) {
					event.consume();
				}
			}
		});
		this.setMinHeight(300);
		this.setMinWidth(550);
	}

	private Pane createRootPane() {
		BorderPane root = new BorderPane();
		tabs = new TabPane();

		Tab tagTab = new Tab();
		tagTab.setText("Tags");
		tagTab.setContent(createTagTable());

		searchResultTab = new Tab();
		searchResultTab.setText("Found Bubbles");
		
		Tab bubbleTab = new Tab();
		bubbleTab.setText("Bubble Overview");
		bubbleTab.setContent(createBubbleTable());

		tabs.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
		tabs.getTabs().addAll(tagTab, searchResultTab, bubbleTab);

		// Setting elements of the root pane
		root.setCenter(tabs);
		root.setBottom(createButtonPane());

		root.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		return root;

	}

	/**
	 * Sets the title of the MainView.
	 */
	private void setTitleBinding() {
		StringBinding binding = new StringBinding() {
			{
				super.bind(model.getBubbleListProperty());
			}

			@Override
			protected String computeValue() {
				return "BrowseMelodie" + "                           (* ^ ω ^)ノ～ "
						+ model.getBubbleListProperty().getSize() + " bubbles";
			}
		};
		this.titleProperty().bind(binding);
	}

	/**
	 * Opens an error alert for the user.
	 * 
	 * @param message
	 *            - displayed to the user.
	 */
	public void showErrorMessage(String message) {
		Alert alert = new Alert(AlertType.ERROR, message);
		alert.showAndWait();
	}

	public void showInfoMessage(String message) {
		Alert alert = new Alert(AlertType.INFORMATION, message);
		alert.showAndWait();
	}

	/**
	 * Opens a alert window to ask the user for confirmation and return the result.
	 * 
	 * @param message
	 *            - displayed to the user.
	 * @return True if the user presses the OK button.<br>
	 *         else False
	 */
	public boolean showConfirmationAlert(String message) {
		Alert alert = new Alert(AlertType.CONFIRMATION, message);
		Optional<ButtonType> result = alert.showAndWait();
		if (result.isPresent() && result.get() == ButtonType.OK) {
			return true;
		}
		return false;
	}

	/**
	 * Opens a file chooser and returns the path of the selected file.
	 * 
	 * @return Absolute path of the selected file.
	 */
	public String showFileDialog() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Mp3", "*.mp3"),
				new FileChooser.ExtensionFilter("Mp2", "*.mp2"), new FileChooser.ExtensionFilter("All Audios", "*.*"));

		File choice = fileChooser.showOpenDialog(this);
		if (choice == null) {
			return null;
		} else {
			return choice.getAbsolutePath();
		}
	}

	/**
	 * Displays the view for adding new Tags.
	 */
	private void showAddTagWindow() {
		TagViewController tagViewController = new TagViewController(model, this);
		tagViewController.createView();
	}

	/**
	 * Updates the Tag Tab after a tag gets added or removed.
	 * 
	 * @param tag
	 *            the modified tag
	 */
	public void updateTagTab() {
			tagGrid.getChildren().clear();

			List<String> tags = model.getTags();

			for (int i = 0; i < tags.size(); i++) {
				Button button = new Button(tags.get(i));
				MainView mv = this;
				button.setOnMouseClicked(new EventHandler<MouseEvent>() {
					public void handle(MouseEvent ME) {
						if (ME.getButton() == MouseButton.PRIMARY) {
							controller.searchBubbleByTag(tags);
						}
						else if (ME.getButton() == MouseButton.SECONDARY) {
							DeleteTagViewController deleteTagViewController = new DeleteTagViewController(model, mv);
							deleteTagViewController.createView(button.getText());
						}
					}
				});

				// Puts all tags in order of a dynamically sized YxY matrix
				tagGrid.add(button, i % (int) Math.ceil(Math.sqrt(tags.size())),
						i / (int) Math.ceil(Math.sqrt(tags.size())));
			}
	};

	private Node createButtonPane() {

		Button deleteBubbleButton = new Button("Delete selected Bubble");
		deleteBubbleButton.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				controller.deleteSelectedBubble();
			}
		});

		Button newBubbleButton = new Button("Add Bubble");
		newBubbleButton.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				controller.importBubbleDialog();
			}
		});

		Button addTagButton = new Button("add Tag");
		addTagButton.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				showAddTagWindow();
			}
		});

		AnchorPane anchorPane = new AnchorPane();
		HBox box = new HBox();
		box.getChildren().addAll(deleteBubbleButton, addTagButton);
		box.setSpacing(10);
		box.setPadding(new Insets(5.0));
		HBox box2 = new HBox();
		box2.getChildren().addAll(newBubbleButton);
		box2.setPadding(new Insets(5.0));
		anchorPane.getChildren().addAll(box, box2);
		AnchorPane.setLeftAnchor(box, 10.0);
		AnchorPane.setRightAnchor(box2, 10.0);

		return anchorPane;
	}

	/**
	 * updates the Found Bubbles Tab of the main view.
	 * @param tags
	 */
	public void updateResultTable(List<String> tags){
		BorderPane resultRoot = new BorderPane();
		resultRoot.setPadding(new Insets(10,10,10,10));
		resultGrid = new GridPane();
		resultGrid.setHgap(30);
		resultGrid.setVgap(30);
		resultGrid.setPadding(new Insets(10, 10, 10, 10));

		SimpleListProperty<Bubble> results = model.getBubbleListProperty();

		Button button = new Button();
		List<Button> buttons = new LinkedList<>();
		for (int i = 0; i < tags.size(); i++) {
			int c = 0;
			int d = 0;
			for(Bubble bubble : results) {

				String toolTip="";
				c++;
				Set<String> tagCheck = bubble.getTags();
				//iff tag is in tags of the bubble, create a btn with its title
				for(String t : tagCheck) {
					toolTip += t + " ";
					if(t.equals(tags.get(i))) {
						button = new Button(bubble.getTitle());
						buttons.add(button);
						// Puts all tags in order of a dynamically sized YxY matrix
					} else {d-=1;}
				}

				// iff tag is in tags of the bubble, add it to Gridpane
				for(Button b : buttons) {
					if(bubble.getTags().contains(tags.get(i))) {
						Button btn = addToolTip(bubble, toolTip);
						resultGrid.add(btn, (c-d) % (int) Math.ceil(Math.sqrt(results.size())), (c-1) / (int) Math.ceil(Math.sqrt(results.size())));
					}
					
				}
			}
		}
		
		VBox vbox = new VBox();
		TextArea searchedTags = new TextArea();
		searchedTags.setPrefWidth(100);
		searchedTags.setMaxHeight(100);
		searchedTags.setText(tags.toString());
		searchedTags.setEditable(false);
		searchedTags.setMouseTransparent(true);
		searchedTags.setFocusTraversable(false);
		searchedTags.setFont(new Font("Arial", 16));
		
		duration = new TextArea();
		duration.setPrefWidth(100);
		duration.setMaxHeight(100);
		duration.setText("00:00");
		duration.setEditable(false);
		duration.setMouseTransparent(true);
		duration.setFocusTraversable(false);
		duration.setFont(new Font("Arial", 16));
		
		vbox.setPadding(new Insets(10));
	    vbox.setSpacing(8);
	    vbox.getChildren().addAll(searchedTags, duration);
		
		resultRoot.setCenter(resultGrid);
		resultRoot.setRight(vbox);
		searchResultTab.setContent(resultRoot);
		tabs.getSelectionModel().selectNext();
	}
	
	public void updateBubbleTags(String tag) {
		for (Bubble b : model.getBubbleListProperty()) {
			b.getTags().remove(tag);
			if(b.getTags().isEmpty()) {
				b.getTags().add(model.getTags().get(0));
			}
		}
	}
	
	/**
	 * 
	 * Adds a String as a Tooltip to a Button.
	 * @return button that the tooltip was added to.
	 */
	private Button addToolTip(Bubble bubble, String toolTip) {
		Button button = new Button(bubble.getTitle());
		new ToolTipDefaultsFixer();
		ToolTipDefaultsFixer.setTooltipTimers(0, 5000, 0);
		Tooltip t = new Tooltip(toolTip);
		t.setFont(new Font("Arial", 16));
		t.getStyleClass().add("ttip");
		button.setTooltip(
			    t
			);
		button.setOnMouseClicked(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent ME) {
				if (ME.getButton() == MouseButton.PRIMARY) {
					model.setMediaPlayer(new MediaPlayer(bubble.getAudio()));
					model.getMediaPlayer().play();
					currentlyPlaying = true;
					model.getMediaPlayer().currentTimeProperty().addListener(new ChangeListener<Duration>() {
					    @Override
					        public void changed(ObservableValue<? extends Duration> observable, Duration oldValue, Duration newValue) {
					    	int minutes = (int)model.getMediaPlayer().getCurrentTime().toMinutes();
					    	int seconds = (int)model.getMediaPlayer().getCurrentTime().toSeconds();
					    	duration.setText(Integer.toString(minutes) + ":" + Integer.toString(seconds));
					    }
					});
				} else if (ME.getButton() == MouseButton.SECONDARY) {
					if(currentlyPlaying == true) {
						model.getMediaPlayer().stop();
						currentlyPlaying = false;
					}
				}
			}
		});
		return button;
	}
	
	private Node createTagTable() {
		tagGrid = new GridPane();
		tagGrid.setHgap(10);
		tagGrid.setVgap(10);
		tagGrid.setPadding(new Insets(10, 10, 10, 10));

		List<String> tags = model.getTags();
		MainView mv = this;

		for (int i = 0; i < tags.size(); i++) {
			Button button = new Button(tags.get(i));
			
			button.setOnMouseClicked(new EventHandler<MouseEvent>() {
				public void handle(MouseEvent ME) {
					if (ME.getButton() == MouseButton.PRIMARY) {
						List<String> searchTags = new LinkedList<>();
						 searchTags.add(button.getText());
						controller.searchBubbleByTag(searchTags);
					}
					else if (ME.getButton() == MouseButton.SECONDARY) {
						DeleteTagViewController deleteTagViewController = new DeleteTagViewController(model, mv);
						deleteTagViewController.createView(button.getText());
					}
				}
			});

			// Puts all tags in order of a dynamically sized YxY matrix
			tagGrid.add(button, i % (int) Math.ceil(Math.sqrt(tags.size())), i / (int) Math.ceil(Math.sqrt(tags.size())));
		}

		return tagGrid;
	}

	private Node createBubbleTable() {
		TableView<Bubble> table = new TableView<>();

		// Creating the columns for the table
		TableColumn<Bubble, String> idColumn = new TableColumn<>("Id");
		TableColumn<Bubble, String> authorColumn = new TableColumn<>("Author");
		TableColumn<Bubble, String> titleColumn = new TableColumn<>("Title");
		TableColumn<Bubble, String> tagsColumn = new TableColumn<>("Tags");

		// Adding the columns to the table
		table.getColumns().add(idColumn);
		table.getColumns().add(authorColumn);
		table.getColumns().add(titleColumn);
		table.getColumns().add(tagsColumn);

		// Setting the column size
		idColumn.prefWidthProperty().bind(table.widthProperty().divide(table.getColumns().size()));
		authorColumn.prefWidthProperty().bind(table.widthProperty().divide(table.getColumns().size()));
		titleColumn.prefWidthProperty().bind(table.widthProperty().divide(table.getColumns().size()));
		tagsColumn.prefWidthProperty().bind(table.widthProperty().divide(table.getColumns().size()));

		// Creating the property values for the columns
		PropertyValueFactory<Bubble, String> idColFactory = new PropertyValueFactory<>("id");
		PropertyValueFactory<Bubble, String> authorColFactory = new PropertyValueFactory<>("author");
		PropertyValueFactory<Bubble, String> titleColFactory = new PropertyValueFactory<>("title");
		
		Callback<CellDataFeatures<Bubble, String>, ObservableValue<String>> tagsColFactory = new Callback<CellDataFeatures<Bubble, String>, ObservableValue<String>>() {
			@Override
			public ObservableValue<String> call(CellDataFeatures<Bubble, String> arg0) {
				String result = "";
				Iterator<String> iterator = arg0.getValue().getTags().iterator();
				while (iterator.hasNext()) {
					result += iterator.next();
					if (iterator.hasNext()) {
						result += ", ";
					}
				}
				SimpleStringProperty prop = new SimpleStringProperty(result);
				return prop;
			}
			
		};

		// Setting the column value factories
		idColumn.setCellValueFactory(idColFactory);
		authorColumn.setCellValueFactory(authorColFactory);
		titleColumn.setCellValueFactory(titleColFactory);
		tagsColumn.setCellValueFactory(tagsColFactory);

		
		// Add event for double click on table rows
		table.setRowFactory(tv -> {
			TableRow<Bubble> row = new TableRow<>();
				row.setOnMouseClicked(event -> {
					if (event.getClickCount() == 2 && (!row.isEmpty())) {
						if(!row.isEmpty()) {
							System.out.println(model.getBubbleListProperty().getValue());
						}
					} else if (event.getButton() == MouseButton.SECONDARY) {
						if(!row.isEmpty()) {
							AssociateController aC = new AssociateController(model, this);
							aC.createView(row.getItem());
						}
					}
					
				});
			return row;
		});
		
		// Add listener to change the selection in the model
		table.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Bubble>() {

			@Override
			public void changed(ObservableValue<? extends Bubble> arg0, Bubble oldValue, Bubble newValue) {
				model.setSelectedBubble(newValue);
			}

		});

		table.itemsProperty().bind(model.getBubbleListProperty());

		// Create ScrollPane container
		ScrollPane scrollPane = new ScrollPane();
		scrollPane.setContent(table);
		scrollPane.setFitToHeight(true);
		scrollPane.setFitToWidth(true);
		table.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		scrollPane.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

		return scrollPane;
	}

}